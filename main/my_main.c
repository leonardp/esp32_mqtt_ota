#include "main.h"
#include "shared.h"

void app_main()
{
    uint8_t sha_256[HASH_LEN] = { 0 };
    esp_partition_t partition;
    static const char *TAG = "MAIN APP LAUNCHER";

    ESP_LOGI(TAG, "Startup..");
    ESP_LOGI(TAG, "Free memory: %d bytes", esp_get_free_heap_size());
    ESP_LOGI(TAG, "IDF version: %s", esp_get_idf_version());

    esp_log_level_set("*", ESP_LOG_INFO);
    esp_log_level_set("wifi", ESP_LOG_WARN);
    esp_log_level_set("mqtt_app", ESP_LOG_VERBOSE);
    esp_log_level_set("MQTT_CLIENT", ESP_LOG_VERBOSE);
    esp_log_level_set("TRANSPORT_TCP", ESP_LOG_VERBOSE);
    esp_log_level_set("TRANSPORT_SSL", ESP_LOG_VERBOSE);
    esp_log_level_set("TRANSPORT", ESP_LOG_VERBOSE);
    esp_log_level_set("OUTBOX", ESP_LOG_VERBOSE);

    // get sha256 digest for the partition table
    partition.address   = ESP_PARTITION_TABLE_OFFSET;
    partition.size      = ESP_PARTITION_TABLE_MAX_LEN;
    partition.type      = ESP_PARTITION_TYPE_DATA;
    esp_partition_get_sha256(&partition, sha_256);
    print_sha256(sha_256, "SHA-256 for the partition table: ");

    // get sha256 digest for bootloader
    partition.address   = ESP_BOOTLOADER_OFFSET;
    partition.size      = ESP_PARTITION_TABLE_OFFSET;
    partition.type      = ESP_PARTITION_TYPE_APP;
    esp_partition_get_sha256(&partition, sha_256);
    print_sha256(sha_256, "SHA-256 for bootloader: ");

    // get sha256 digest for running partition
    esp_partition_get_sha256(esp_ota_get_running_partition(), sha_256);
    print_sha256(sha_256, "SHA-256 for current firmware: ");
 
    // initialize SPI
    spi_device_handle_t epd_spi;
    epd_spi = epd_spi_init();

    //Initialize the e-Paper Display
    epd_init(epd_spi);

    // Initialize NVS.
    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        // OTA app partition table has a smaller NVS partition size than the non-OTA
        // partition table. This size mismatch may cause NVS initialization to fail.
        // If this happens, we erase NVS partition and initialize NVS again.
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
    ESP_ERROR_CHECK( err );

    /*
     * TODO: Error checking!
     */

    xQueue = xQueueCreate( 1, sizeof( MqttMsg_t) );

    init_wifi();

    xTaskCreate(
      &mqtt_app, /* Function to implement the task */
      "mqtt_app", /* Name of the task */
      8192, /* Stack size in words */
      ( void * )epd_spi, /* Task input parameter */
      4, /* Priority of the task */
      NULL); /* Task handle. */

    xTaskCreate(
      &epd_app, /* Function to implement the task */
      "epd_app", /* Name of the task */
      512, /* Stack size in words */
      NULL, /* Task input parameter */
      4, /* Priority of the task */
      NULL); /* Task handle. */
    
//    xTaskCreatePinnedToCore(
//      &ota_task, /* Function to implement the task */
//      "ota_task", /* Name of the task */
//      8192, /* Stack size in words */
//      NULL, /* Task input parameter */
//      1, /* Priority of the task */
//      NULL, /* Task handle. */
//      1); /* Core where the task should run */
}
