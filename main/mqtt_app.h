#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "esp_log.h"

#include "graphics.h"
#include "resources.h"
#include "screen.h"

#include "mqtt_client.h"

#include "epd.h"

#define TOPIC_EXCHANGE "custom/exchange"
#define TOPIC_HEARTBEAT "remote/heartbeat"
#define TOPIC_REFRESH "remote/refresh"
#define TOPIC_PP "remote/pp"
#define TOPIC_OTA "remote/ota"


void mqtt_app(void *pvParameter);
