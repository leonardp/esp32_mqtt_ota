#include "epd_app.h"


void epd_app(void *pvParameter)
{
    // heartbeat
    gpio_set_direction(GPIO_NUM_22, GPIO_MODE_OUTPUT);
    const TickType_t xDelay = 1000 / portTICK_PERIOD_MS;
    int state = 0;

    while (true) 
    {
        gpio_set_level(GPIO_NUM_22, state);
        state = !state;
        vTaskDelay(xDelay);

    }

    //const unsigned char *frames[] = {
    //        BITMAP_fractal_1.payload,
    //        BITMAP_fractal_3.payload,
    //        BITMAP_fractal_4.payload,
    //        BITMAP_fractal_5.payload
    //};

    //gfx_init();
    //screen_show_temperatures(12,34);
    //screen_demo_show_fonts();
    //epd_black_fb(epd_spi);

    //printf("frame nr: %d\n", framer);
    //epd_display_fb(epd_spi, frames[framer], NULL);
    //esp_deep_sleep_start();
}
