#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "esp_http_client.h"
#include "esp_ota_ops.h"
#include "esp_log.h"
#include "esp_flash_partitions.h"
#include "esp_partition.h"

#define SERVER_URL CONFIG_FIRMWARE_UPG_URL
#define BUFFSIZE 1024

extern const uint8_t server_cert_pem_start[] asm("_binary_ca_cert_pem_start");
extern const uint8_t server_cert_pem_end[] asm("_binary_ca_cert_pem_end");

void __attribute__((noreturn)) task_fatal_error();
void http_cleanup(esp_http_client_handle_t client);
void ota_task(void *pvParameter);
