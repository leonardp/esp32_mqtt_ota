//#include <stdio.h>
//#include <stdlib.h>
//#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_sleep.h"
#include "nvs_flash.h"
//#include "driver/spi_master.h"
//#include "driver/gpio.h"

#include "types.h"
#include "wifi.h"
#include "mqtt_app.h"
#include "epd_app.h"
#include "ota_app.h"

#define HASH_LEN 32 /* SHA-256 digest length */

RTC_DATA_ATTR int framer;

void RTC_IRAM_ATTR esp_wake_deep_sleep(void) {
    esp_default_wake_deep_sleep();
    framer++;
    if (framer > 5)
    {
      framer = 0;
    }
}

void print_sha256 (const uint8_t *image_hash, const char *label)
{
    static const char *TAG = "DUH";
    char hash_print[HASH_LEN * 2 + 1];
    hash_print[HASH_LEN * 2] = 0;
    for (int i = 0; i < HASH_LEN; ++i) {
        sprintf(&hash_print[i * 2], "%02x", image_hash[i]);
    }
    ESP_LOGI(TAG, "%s: %s", label, hash_print);
}
