#include "wifi.h"
#include "mqtt_app.h"
#include "shared.h"

static esp_err_t mqtt_event_handler(esp_mqtt_event_handle_t event)
{
    esp_mqtt_client_handle_t client = event->client;
    int msg_id;
    static const char *TAG = "MQTT_EXAMPLE";
    char buf[50];

    MqttMsg_t * mqtt_p = malloc(sizeof(*mqtt_p));
    spi_device_handle_t epd_spi = event->user_context;

    switch (event->event_id) {
        case MQTT_EVENT_CONNECTED:
            ESP_LOGI(TAG, "MQTT_EVENT_CONNECTED");
            msg_id = esp_mqtt_client_subscribe(client, "custom/exchange", 0);
            ESP_LOGI(TAG, "sent subscribe successful, msg_id=%d", msg_id);
            msg_id = esp_mqtt_client_subscribe(client, "remote/refresh", 0);
            ESP_LOGI(TAG, "sent subscribe successful, msg_id=%d", msg_id);
            msg_id = esp_mqtt_client_subscribe(client, "remote/pp", 0);
            ESP_LOGI(TAG, "sent subscribe successful, msg_id=%d", msg_id);
            msg_id = esp_mqtt_client_subscribe(client, "remote/ota", 0);
            ESP_LOGI(TAG, "sent subscribe successful, msg_id=%d", msg_id);
            break;
        case MQTT_EVENT_DISCONNECTED:
            ESP_LOGI(TAG, "MQTT_EVENT_DISCONNECTED");
            break;
        case MQTT_EVENT_SUBSCRIBED:
            ESP_LOGI(TAG, "MQTT_EVENT_SUBSCRIBED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_UNSUBSCRIBED:
            ESP_LOGI(TAG, "MQTT_EVENT_UNSUBSCRIBED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_PUBLISHED:
            ESP_LOGI(TAG, "MQTT_EVENT_PUBLISHED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_DATA:
            ESP_LOGI(TAG, "MQTT_EVENT_DATA");
            //memset( event->topic, '\0', sizeof(char)*event->topic_len );
            printf("topic=%.*s\r\n", event->topic_len, event->topic);
            printf("msg=%.*s\r\n", event->data_len, event->data);
            memcpy( &buf , event->topic, event->topic_len );
            buf[event->topic_len] = '\0';

            /*
            if(event->data_len) {
                mqtt_p->msg= event->data;
                mqtt_p->msg[event->data_len] = '\0';
                printf("DATA=%.*s\r\n", event->data_len, event->data);
                printf("msg=%s\r\n", mqtt_p->msg);
            }
            */

            if ( strcmp( TOPIC_REFRESH, buf ) == 0 ) {
                ESP_LOGI(TAG, "refreshing epd...");
                epd_clear(epd_spi);
                epd_black_fb(epd_spi);
            }
            if ( strcmp( TOPIC_EXCHANGE, buf ) == 0 ) {
                ESP_LOGI(TAG, "topic exchange received...");
                memcpy( &buf , event->data, event->data_len );
                buf[event->data_len] = '\0';
                gfx_init();
                gfx_write_text(&FONT_roboto_condensed_regular_14, 200, 0, buf);
            }
            if ( strcmp( TOPIC_PP, buf ) == 0 ) {
                gfx_init();
                gfx_vline(10);
                gfx_hline(32);
            }
            if ( strcmp( TOPIC_OTA, buf) == 0 ) {
                ESP_LOGI(TAG, "Starting OTA task");
            }
            break;
        case MQTT_EVENT_ERROR:
            ESP_LOGI(TAG, "MQTT_EVENT_ERROR");
            break;
    }
    return ESP_OK;
}

void mqtt_app(void *pvParameter)
{
    static const char *TAG = "mqtt_app";
    int msg_id;
    int state = 0;
    char msg;
    const TickType_t xDelay = 1000 / portTICK_PERIOD_MS;

    esp_mqtt_client_config_t mqtt_cfg = {
        .uri = CONFIG_BROKER_URL,
        .event_handle = mqtt_event_handler,
        .user_context = (void *)pvParameter,
    };

    esp_mqtt_client_handle_t client = esp_mqtt_client_init(&mqtt_cfg);

    xEventGroupWaitBits(wifi_event_group, CONNECTED_BIT,
                        false, true, portMAX_DELAY);

    esp_mqtt_client_start(client);

    /*
    while ( xQueueReceive( xQueue, &( mqtt_p ), 0 ) )
    {
      printf("topic=%.*s\r\n", mqtt_p->topic_len, mqtt_p->topic);
      printf("msg=%.*s\r\n", mqtt_p->msg_len, mqtt_p->msg);
      //printf("DATA=%d\r\n", mqtt_p->msg_len);
      gfx_init();
      gfx_write_text(&FONT_roboto_condensed_regular_14, 200, 0, mqtt_p->msg);
      epd_black_fb(epd_spi);
      epd_refresh(epd_spi);
      epd_busy_wait();
    }
    */

    for( ;; )
    {
      msg = '0'+state;
      msg_id = esp_mqtt_client_publish(client, "remote/heartbeat", &msg, 0, 0, 0);
      state = !state;
      ESP_LOGI(TAG, "sent publish successful, msg_id=%d", msg_id);
      //ESP_LOGI(TAG, "alive");
      vTaskDelay(xDelay);
    }
}
