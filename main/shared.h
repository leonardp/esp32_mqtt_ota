#include "freertos/queue.h"

typedef struct 
{
  char *topic;
  //char topic[50];
  int topic_len;
  char *msg;
  int  msg_len;
} MqttMsg_t;

//static or die ?!
// -> static problematic?
// -> non static -> crash
//static MqttMsg_t *mqtt_p;

QueueHandle_t xQueue;
