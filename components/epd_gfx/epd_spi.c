#include "epd.h"

spi_device_handle_t epd_spi_init(void)
{
  esp_err_t ret;

  spi_device_handle_t spi;
  spi_bus_config_t buscfg={
      .miso_io_num=PIN_NUM_MISO,
      .mosi_io_num=PIN_NUM_MOSI,
      .sclk_io_num=PIN_NUM_CLK,
      .quadwp_io_num=-1,
      .quadhd_io_num=-1,
      .max_transfer_sz=MAX_TRANSFER_SIZE
  };
  spi_device_interface_config_t devcfg={
      .clock_speed_hz=SPI_CLOCK_SPEED,
      .mode=0,
      .spics_io_num=PIN_NUM_CS,
      .queue_size=1,
      .pre_cb=epd_spi_pre_transfer_callback,  //Specify pre-transfer callback to handle D/C line
  };

  //Initialize non-SPI GPIOs
  gpio_set_direction(PIN_NUM_DC, GPIO_MODE_OUTPUT);
  gpio_set_direction(PIN_NUM_RST, GPIO_MODE_OUTPUT);
  gpio_set_direction(PIN_NUM_BUSY, GPIO_MODE_INPUT);

  //Initialize the SPI bus
  //ret=spi_bus_initialize(HSPI_HOST, &buscfg, o);
  ret=spi_bus_initialize(HSPI_HOST, &buscfg, 2);
  ESP_ERROR_CHECK(ret);

  //Attach the Display to the SPI bus
  ret=spi_bus_add_device(HSPI_HOST, &devcfg, &spi);
  ESP_ERROR_CHECK(ret);

  return spi;
}

void epd_spi_pre_transfer_callback(spi_transaction_t *t)
{
    int dc=(int)t->user;
    gpio_set_level(PIN_NUM_DC, dc);
}

void epd_spi_cmd(spi_device_handle_t spi, const uint8_t cmd)
{
    esp_err_t ret;
    spi_transaction_t t;
    memset(&t, 0, sizeof(t));       //Zero out the transaction
    t.length=8;                     //Command is 8 bits
    t.tx_buffer=&cmd;               //The data is the cmd itself
    t.user=(void*)0;                //D/C needs to be set to 0
    ret=spi_device_transmit(spi, &t);  //Transmit!
    assert(ret==ESP_OK);            //Should have had no issues.
}

void epd_spi_data(spi_device_handle_t spi, const uint8_t *data, uint32_t len)
{
  esp_err_t ret;
  spi_transaction_t t;
  if (len==0) return;             //no need to send anything
  memset(&t, 0, sizeof(t));       //Zero out the transaction
  t.length=len*8;                 //Len is in bytes, transaction length is in bits.
  t.tx_buffer=data;               //Data
  t.user=(void*)1;                //D/C needs to be set to 1
  ret=spi_device_transmit(spi, &t);  //Transmit!
  assert(ret==ESP_OK);            //Should have had no issues.
}

void epd_spi_fb(spi_device_handle_t spi)
{
  esp_err_t ret;
  spi_transaction_t t;

  memset(&t, 0, sizeof(t));       //Zero out the transaction
  t.tx_buffer=&GLOBAL_framebuffer;//Data
  t.length=MAX_TRANSFER_SIZE*8;     //lenth of buffer in bytes!
  t.user=(void*)1;                //D/C needs to be set to 1
  ret=spi_device_transmit(spi, &t);//Transmit!
  assert(ret==ESP_OK);            //Should have had no issues.
}

void epd_reset(void)
{
    gpio_set_level(PIN_NUM_RST, 0);
    vTaskDelay(100 / portTICK_RATE_MS);
    gpio_set_level(PIN_NUM_RST, 1);
    vTaskDelay(100 / portTICK_RATE_MS);
}

void epd_busy_wait(void)
{
   // 0: busy, 1: idle
  while ( gpio_get_level(PIN_NUM_BUSY) == 0 )
  {
    vTaskDelay(100 / portTICK_RATE_MS);
  }
  //TODO: add sleep mode and edge sensing interrupt
}

