#include "epd.h"

void epd_init(spi_device_handle_t spi)
{
  epd_reset();
  epd_send_sequence(spi, epd_init_cmds);
  epd_busy_wait();
  epd_send_sequence(spi, epd_config_cmds);
  
}

void epd_clear(spi_device_handle_t spi)
{
  const uint8_t pixel_off = 0xFF;
  const uint8_t pixel_on  = 0x00;
  
  epd_spi_cmd(spi, DATA_START_TRANSMISSION_1);
  
  for(int i = 0; i < DISPLAY_WIDTH / 8 * DISPLAY_HEIGHT; i++)
  {
    epd_spi_data(spi, &pixel_off, 1);
  }
  
  epd_spi_cmd(spi, DATA_START_TRANSMISSION_2);
  
  for(int i = 0; i < DISPLAY_WIDTH / 8 * DISPLAY_HEIGHT; i++)
  {
    epd_spi_data(spi, &pixel_off, 1);
  }
}

void epd_send_sequence(spi_device_handle_t spi, const epd_cmd_seq_t *cmds)
{
  uint8_t cmd = 0;

  while (cmds[cmd].databytes!=0xff) 
  {
    epd_spi_cmd(spi, cmds[cmd].cmd);
    epd_spi_data(spi, cmds[cmd].data, cmds[cmd].databytes&0x1F);

    if (cmds[cmd].databytes&0x80) 
    {
      vTaskDelay(100 / portTICK_RATE_MS);
    }
    cmd++;
  }
}

void epd_display_static(spi_device_handle_t spi, const unsigned char* frame_black, const unsigned char* frame_red)
{
  if (frame_black != NULL)
  {
    epd_spi_cmd(spi, DATA_START_TRANSMISSION_1);

    for(int i = 0; i < DISPLAY_WIDTH / 8 * DISPLAY_HEIGHT; i++)
    {
      epd_spi_data(spi, &frame_black[i], 1);
    }
  }
  if (frame_red != NULL)
  {
    epd_spi_cmd(spi, DATA_START_TRANSMISSION_2);

    for(int i = 0; i < DISPLAY_WIDTH / 8 * DISPLAY_HEIGHT; i++)
    {
      epd_spi_data(spi, &frame_red[i], 1);
    }
  }
  epd_spi_cmd(spi, DISPLAY_REFRESH);
  epd_busy_wait();
}

void epd_black_fb(spi_device_handle_t spi)
{
  epd_spi_cmd(spi, DATA_START_TRANSMISSION_1);
  epd_spi_fb(spi);
  epd_spi_cmd(spi, DISPLAY_REFRESH);
  epd_busy_wait();
}

void epd_red_fb(spi_device_handle_t spi)
{
  epd_spi_cmd(spi, DATA_START_TRANSMISSION_2);
  epd_spi_fb(spi);
  epd_spi_cmd(spi, DISPLAY_REFRESH);
  epd_busy_wait();
}

void epd_refresh(spi_device_handle_t spi)
{
   epd_spi_cmd(spi, DISPLAY_REFRESH);
}

void epd_deinit(void)
{
}
